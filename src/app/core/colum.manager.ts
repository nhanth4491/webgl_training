import { Scene, Mesh, BufferGeometry, Vector3, Object3D, Color, Material, MeshPhongMaterial, DoubleSide, Geometry, LineSegments, BoxBufferGeometry } from "three";
import { Util } from "./utils";
import { AppComponent } from "../app.component";
import { GeometryManager } from "./geometry.manager";
import { MaterialManager } from "./material.manager";
import { GeometryInfo, Printing2DLine, ViewType, LineType, Printing2DGeometry, Printing2DGeometryType } from './models';
import { CONFIG as env, GEOMETRY_TYPE } from '../app.config';
import { of } from 'rxjs';

export class ColumManager {
    private scene: Scene;
    private APP: AppComponent;

    private utils: Util;

    private geometryManager: GeometryManager;
//Tao phoi
    public geo_colum: GeometryInfo;
    public geo_purlin: GeometryInfo;
//Material  
    private material: Material;

    private geoColumnHeight: number = 4000;
    private geoColumnWidth: number = 500;
    private geoColumnLength: number = 500;
    constructor() {
        this.utils = new Util();
        this.geometryManager = GeometryManager.Instance();
    }

    public init(app: AppComponent): Promise<void> {
        return new Promise((resolve, reject) => {
            this.APP = app;
            this.scene = app.scene;
            //this.material = MaterialManager.Instance().DEFAULT.clone();     
            this.material = new MeshPhongMaterial({color: new Color('Red')});   
            this.APP.sldEaveWidth.addAction(this.update.bind(this));  
            this.APP.sldExistingWallHeight.addAction(this.update.bind(this));
            this.APP.sldExistingLength.addAction(this.update.bind(this));
            resolve();
        });
    }
    public optimize() : Promise<void> {
        return new Promise((resolve, reject) => {

            this.geo_colum = new GeometryInfo();
            this.geo_colum.geometry = this.geometryManager.COLUMN.S63x3.geometry.clone();
            this.geo_colum.width = this.geometryManager.COLUMN.S63x3.width;
            this.geo_colum.length = this.geometryManager.COLUMN.S63x3.length;
            this.geo_colum.height = this.geometryManager.COLUMN.S63x3.height;
            
            //this.geo_colum.geometry = new BoxBufferGeometry(this.geoColumnWidth, this.geoColumnHeight, this.geoColumnLength);
            this.geo_colum.geometry.rotateX(Math.PI/2);

            this.geo_colum.geometry.translate(0, this.geometryManager.COLUMN.S63x3.length/2, 0);


            this.geo_purlin = new GeometryInfo();
            this.geo_purlin.geometry = this.geometryManager.PURLIN.TH22.geometry.clone();
            this.geo_purlin.width = this.geometryManager.PURLIN.TH22.width;
            this.geo_purlin.length = this.geometryManager.PURLIN.TH22.length;
            this.geo_purlin.height = this.geometryManager.PURLIN.TH22.height;


            
            
            // .clone()
            // .rotateY(Math.PI / 2)
            // .translate(0, translateHeight, translateLength);
            // this.geo_colum.width = this.geometryManager.EXISTING_WALL.EXISTING_WALL.width;
            // this.geo_colum.length = this.geometryManager.EXISTING_WALL.EXISTING_WALL.length;
            // this.geo_colum.height = this.geometryManager.EXISTING_WALL.EXISTING_WALL.height;

            
            resolve();
        });
    }
    public load(): Promise<void> {
        return new Promise((resolve, reject) => {
            this.scene.remove(...this.scene.children.filter(x=> x.userData.type == 'Colum'));
            // this.scene.remove(...this.scene.children.filter(x => x.userData.type == GEOMETRY_TYPE.EXISTING_WALL));
            // this.scene.remove(...this.scene.children.filter(x => x.userData.type == "EXISTING_WALL_OUTLINE"));
            
            let width = this.APP.sldEaveWidth.currentValue/2;
            let height = this.APP.sldExistingWallHeight.currentValue;
            let length = this.APP.sldExistingLength.currentValue/2;


            // let purlin = new Mesh(this.geo_purlin.geometry, this.material);
            // this.scene.add(purlin);

            
            let soluongCot = this.soluongo(length*2);
            let baySize = length*2/soluongCot;
            let offsetZ = -length;


            for(let i=0; i <= soluongCot/2; i++){                
                this.addColumn(offsetZ,width);
                this.addColumn(offsetZ,-width);
                offsetZ += baySize;
            }
            // if((-offsetZ)-baySize*soluongCot<2000 && (-offsetZ)-baySize*soluongCot>500){
            //     this.addColumn(0,width);
            //     this.addColumn(0,-width);
            // }
            // let colum1 = new Mesh(this.geo_colum.geometry, this.material);
            // colum1.userData = {type:'Colum' };           
            // colum1.scale.set(1, height/this.geo_colum.length, 1);
            // colum1.translateZ(-length);
            // colum1.translateX(-width);

            // let colum2 = new Mesh(this.geo_colum.geometry, this.material);
            // colum2.userData = {type:'Colum' };
            // colum2.scale.set(1, height/this.geo_colum.length, 1);
            // colum2.translateZ(-length);
            // colum2.translateX(width);

            // let colum3 = new Mesh(this.geo_colum.geometry, this.material);
            // colum3.userData = {type:'Colum' };
            // colum3.scale.set(1, height/this.geo_colum.length, 1);
            // colum3.translateZ(length);
            // colum3.translateX(width);

            // let colum4 = new Mesh(this.geo_colum.geometry, this.material);
            // colum4.userData = {type:'Colum' };
            // colum4.scale.set(1, height/this.geo_colum.length, 1);
            // colum4.translateZ(length);
            // colum4.translateX(-width);
            
            // this.scene.add(colum1,colum2,colum3,colum4);
            resolve();
        });
    }
    private soluongo(X:number):number{
        let soluong = 1;
        while(soluong*2000<X)
            soluong++;
        return soluong;
    }
    private update(preVal:number,curVal:number) {
        this.load();
    }
    public addColumn(offsetX: any, X: any) {
        let colum1 = new Mesh(this.geo_colum.geometry, this.material);
        colum1.userData = {type:'Colum' };           
        colum1.scale.set(1, this.APP.sldExistingWallHeight.currentValue/this.geo_colum.length, 1);
        colum1.translateX(-X);
        colum1.translateZ(offsetX);
        this.scene.add(colum1);
        
        let colum2 = new Mesh(this.geo_colum.geometry, this.material);
        colum2.userData = {type:'Colum' };           
        colum2.scale.set(1, this.APP.sldExistingWallHeight.currentValue/this.geo_colum.length, 1);
        colum2.translateX(-X);
        colum2.translateZ(-offsetX);
        this.scene.add(colum2);
    }

    public uiChanged(preVal: number, curVal): void {
        this.load();
    }
}